import uuid
from typing import Dict, Union

import sqlite3

from core.commons import constants
from core.device.model.Device import Device
from core.device.model.DeviceAbility import DeviceAbility
from core.webui.model.DeviceClickReactionAction import DeviceClickReactionAction
from core.webui.model.OnDeviceClickReaction import OnDeviceClickReaction

from ..Sonos import Sonos as sonosSkill

class Sonos(Device):

	@classmethod
	def getDeviceTypeDefinition(cls) -> dict:
		return {
			'deviceTypeName'        : 'Sonos',
			'perLocationLimit'      : 0,
			'totalDeviceLimit'      : 0,
			'allowLocationLinks'    : True,
			'allowHeartbeatOverride': False,
			'heartbeatRate'         : 30,
			'deviceSettings'        : dict(),
			'abilities'             : [DeviceAbility.NONE]
		}


	def __init__(self, data: Union[sqlite3.Row, Dict]):
		super().__init__(data)


	def onUIClick(self) -> dict:
		if not self.skillInstance:
			return dict()

		skill: sonosSkill = self.skillInstance
		players = skill.sonosPlayers
		if not self.paired:
			reaction = OnDeviceClickReaction(
				action=DeviceClickReactionAction.LIST_SELECT.value,
				data={
					'title': skill.randomTalk(text='deviceSelection'),
					'body': skill.randomTalk(text='selectDeviceInList'),
					'list': list(players.keys())
				},
				reply={
					'secret': self.newSecret(),
					'concerns': 'pairing'
				}
			)

			return reaction.toDict()
		else:
			for playername, player in players.items():
				if player.ip_address == self.getParam('ip'):
					if skill.isPlaying(player):
						player.stop()
					else:
						player.play()
					break

		return dict()


	def onDeviceUIReply(self, data: dict):
		if not self.checkSecret(secret=data.get('secret', constants.UNKNOWN)):
			return

		if data.get('concerns', '') == 'pairing':
			answer = data.get('answer', '')
			for playername, player in self.skillInstance.sonosPlayers.items():
				if playername == answer:
					self.updateParam('ip', player.ip_address)
					self.pairingDone(uid=str(uuid.uuid4()))
					return
